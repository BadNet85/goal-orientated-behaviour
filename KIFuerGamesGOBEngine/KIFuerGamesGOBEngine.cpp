// KIFuerGamesGOBEngine.cpp: Definiert den Einstiegspunkt für die Konsolenanwendung.
//

#include "stdafx.h"
#include "GoalEngine.h"
#include "Action.h"
#include <time.h>
#include <ctime>
#include <thread>
#include <chrono>
#include <ratio>
#include <stdio.h>
#include <curses.h>
#include "Goal.h"
#include "ScreenMngr.h"

using namespace std;
using namespace std::chrono;
using namespace std::this_thread;



int main()
{
	high_resolution_clock clock;
	ScreenMngr* mngr = new ScreenMngr();
	GoalEngine goalEngine(mngr);
	auto tempTime1 = clock.now();
	auto tempTime2 = clock.now();
	initscr();
	cbreak();
	noecho();
	curs_set(0);

#pragma region Goals

	Goal hunger("Hunger", 0.3f, 0.8f, 0.008f);
	Goal tired("Muedigkeit", 0.1f, 0.7f, 0.009f);
	Goal fun("Spass", 0.5f, 0.75f, 0.01f);
	Goal thirst("Durst", 0.2f, 0.6f, 0.0095f);
	Goal money("Geld", 0.1f, 0.9f, 0.0f);
	goalEngine.addGoal(hunger);
	goalEngine.addGoal(tired);
	goalEngine.addGoal(fun);
	goalEngine.addGoal(thirst);
	goalEngine.addGoal(money);

#pragma endregion

#pragma region Actions

	Action doener("Doener Kaufen", 10, 8, 22);
	doener.AddGoalEffect("Hunger", -0.15f);
	doener.AddGoalEffect("Geld", 0.03f);
	doener.AddGoalEffect("Spass", -0.015f);
	doener.AddGoalEffect("Muedigkeit", 0.06f);

	Action sleep("Schlafen", 20, 23, 8);
	sleep.AddGoalEffect("Muedigkeit", -0.06f);
	sleep.AddGoalEffect("Hunger", 0.03f);
	sleep.AddGoalEffect("Durst", 0.03f);

	Action zocken("Zocken", 30, 0, 24);
	zocken.AddGoalEffect("Spass", -0.03f);
	zocken.AddGoalEffect("Muedigkeit", -0.024f);

	Action drink("Trinken", 5, 0, 24);
	drink.AddGoalEffect("Durst", -0.15f);

	Action work("Arbeiten", 30, 8, 20);
	work.AddGoalEffect("Geld", -0.15f);
	work.AddGoalEffect("Muedigkeit", 0.045f);
	work.AddGoalEffect("Hunger", 0.03f);

	Action snack("Snack", 5, 0, 24);
	snack.AddGoalEffect("Hunger", -0.03f);

	Action relax("Entspannen", 15,8,23);
	relax.AddGoalEffect("Spass", 0.036f);
	relax.AddGoalEffect("Muedigkeit", -0.03f);

	Action beg("Betteln", 20, 0, 7);
	beg.AddGoalEffect("Geld", -0.03f);
	beg.AddGoalEffect("Spass", 0.12f);


	goalEngine.addAction(beg);
	goalEngine.addAction(doener);
	goalEngine.addAction(sleep);
	goalEngine.addAction(zocken);
	goalEngine.addAction(drink);
	goalEngine.addAction(work);
	goalEngine.addAction(snack);
	goalEngine.addAction(relax);


#pragma endregion


	while (true) 
	{
		chrono::duration<float, std::milli> timeDelta = (tempTime2 - tempTime1);
		tempTime1 = clock.now();
		goalEngine.Update((timeDelta.count() / 1000)*3);
		tempTime2 = clock.now();
		goalEngine.SetMode(mngr->GetIntput());
	}
	endwin();
}



