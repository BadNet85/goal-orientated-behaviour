#include "ScreenMngr.h"


ScreenMngr::ScreenMngr() {
	initscr();
	_timeWin = newwin(1, 40, 0, 0);
	_toDoWin = newwin(15, 50, 10, 0);
	_goalsWin = newwin(15, 55, 1, 0);
	_availWin = newwin(15, 20, 10, 50);
	_modeWin = newwin(1, 30, 0, 30);
	_discontentWin = newwin(9,50,1,56);
	keypad(_modeWin, true);
	_modeWin->_nodelay = true;
	box(_discontentWin, 0, 0);
}


ScreenMngr::~ScreenMngr() {}

void ScreenMngr::PrintTime(vector<int> time) 
{
	wmove(_timeWin, 0, 0);
	string Hrs;
	string Min;
	if (time[0] < 10) {
		Hrs = "0";
		Hrs.append(to_string(time[0]));
	} else
		Hrs = to_string(time[0]);

	if (time[1] < 10) {
		Min = "0";
		Min.append(to_string(time[1]));
	} else
		Min = to_string(time[1]);
	string clock;
	clock.append(Hrs);
	clock.append(":");
	clock.append(Min);
	string day;
	day.append(to_string(time[2]));
	wprintw(_timeWin,"Uhrzeit: %s\t Tag: %s", clock.c_str(),day.c_str());
	wrefresh(_timeWin);
}

void ScreenMngr::PrintToDo(vector<Action> toDo, Action* active) 
{
	wmove(_toDoWin, 0, 0);
	if (active != nullptr)
		wprintw(_toDoWin,"Aktive Aktion: %s \t Aktiv fuer: %i\n", active->GetName().c_str(), active->TimeLeft());
	else
		wprintw(_toDoWin, "Aktive Aktion: \n");
	wprintw(_toDoWin, "Todo: \n");
	wclrtobot(_toDoWin);
	for (auto itr = toDo.begin(); itr != toDo.end(); itr++) {
		wprintw(_toDoWin, "%s\n", itr->GetName().c_str());
	}
	wrefresh(_toDoWin);
}

void ScreenMngr::PrintGoals(vector<Goal> goals) 
{
	wmove(_goalsWin, 0, 0);
	wprintw(_goalsWin,"Goals:\n");
	for (auto itr = goals.begin(); itr != goals.end(); itr++) {
		int number = (itr->GetValue() * 30);
		string temp = string(number, '=');
		string temp2 = string(30 - number, ' ');
		wprintw(_goalsWin,"%s", itr->GetName().c_str());
		wmove(_goalsWin,getcury(_goalsWin), 15);
		wprintw(_goalsWin,"\t[%s>%s] \n", temp.c_str(), temp2.c_str());
	}
	wrefresh(_goalsWin);


}

void ScreenMngr::PrintAvailable(vector<Action> available) 
{
	wmove(_availWin, 0, 0);
	int counter = 0;
	wprintw(_availWin,"Verfuegbare Aktionen:\n");
	wclrtobot(_availWin);
	for (auto itr = available.begin(); itr != available.end(); itr++) 
	{
		counter++;
		wmove(_availWin,counter, 0);
		wprintw(_availWin,"%s \n", itr->GetName().c_str());
	}
	wrefresh(_availWin);
}

void ScreenMngr::PrintMode(Mode mode) 
{
	string temp;
	wmove(_modeWin, 0, 0);
	wclrtoeol(_modeWin);
	switch (mode) 
	{
		case(Critical):
		{
			temp.append("Dringenste");
			break;
		}
		case(Discontent):
		{
			temp.append("Unzufriedenheit");
			break;
		}
		case(DiscontentTime):
		{
			temp.append("Unzufriedenheit Zeit");
			break;
		}

	}
	wprintw(_modeWin, "Modus: %s", temp.c_str());
	wrefresh(_modeWin);
}

void ScreenMngr::PrintDiscontent(float dis, vector<Goal*> krit, Action* best, float reduction) 
{
	if (!disEnabled) 
	{
		wclear(_discontentWin);
		return;
	}
	wmove(_discontentWin, 0, 0);
	wclrtobot(_discontentWin);
	wprintw(_discontentWin, "Aktuelle Unzufriedenheit: %f", dis);
	wmove(_discontentWin, 2, 0);
	string bestName = " ";
	if (best != nullptr) 
	{ 
		bestName = best->GetName().c_str();
	}
	wprintw(_discontentWin, "Beste Aktion: %s \n Verringert um: %f",bestName.c_str(), reduction);
	wmove(_discontentWin, 5, 0);
	wprintw(_discontentWin, "Kritische Beduerfnisse: \n");
	for (auto itr = krit.begin(); itr != krit.end(); itr++) {
		wprintw(_discontentWin, "%s Wert: %f\n", (*itr)->GetName().c_str(), (*itr)->GetValue());
	}
	wrefresh(_discontentWin);

}

int ScreenMngr::GetIntput() 
{
	int c = 0;
	c = wgetch(_modeWin);
	wrefresh(_modeWin);
	int temp = c;
	return c;
}

void ScreenMngr::EnableDiscontentWindow(bool value) 
{
	disEnabled = value;
}
