#include "stdafx.h"
#include "Action.h"


Action::Action(string name, float duration, int availableFrom, int availableTo)
{
	_name = name;
	_duration = duration;
	_availableFrom = availableFrom;
	_availableTo = availableTo;
}

Action::~Action() {}

void Action::AddGoalEffect(string goal, float effect) 
{
	_goalEffects.insert_or_assign(goal, effect);
}

void Action::Update(float timeDelta) 
{
	_stopWatch += timeDelta*3;
}

void Action::start(float startTime) 
{
	_startingTime = startTime;
}

float Action::GetEffectForGoal(string goal) {
	if (_goalEffects.find(goal) != _goalEffects.end())
		return _goalEffects.at(goal);
	else
		return 0.0f;
}

int Action::TimeLeft() {
	return _duration - _stopWatch;
}

bool Action::IsDone() {
	return _stopWatch >= _duration;
}

string Action::GetName() {
	return _name;
}

bool Action::operator==(Action  other) {
	return this->GetName() == other.GetName();
}

bool Action::HasEffectOn(string goal) 
{
	if (_goalEffects.find(goal) != _goalEffects.end())
	{
		return true;
	} else
		return false;
}

bool Action::IsAvailable(vector<int> time) {

	if (_availableTo > _availableTo) {
		if (time[0] >= _availableFrom || time[0] <= _availableTo)
			return true;
		else
			return false;
	}
	else 
	{
		if (time[0] >= _availableFrom && time[0] <= _availableTo)
			return true;
		else
			return false;
	}
}

void Action::SetDuration(float dur) 
{
	_duration = dur;
}

float Action::GetDuration() {
	return _duration;
}

vector<float> Action::GetGoalEffects() 
{
	vector<float> temp;
	for (auto itr = _goalEffects.begin(); itr != _goalEffects.end(); itr++) 
	{
		temp.push_back(itr->second);
	}
	return temp;
}
