#include "stdafx.h"
#include "Goal.h"


Goal::Goal(string name, float start, float threshold, float rateOfChange) 
{
	_name = name;
	_value = start;
	_threshold = threshold;
	_rateOfChange = rateOfChange;
	_defaultRateOfChange = rateOfChange;

}

Goal::~Goal() {}

string Goal::GetName() {
	return _name;
}

float Goal::GetThreshold() {
	return _threshold;
}

float Goal::GetRateOfChange() {
	return _rateOfChange;
}

float Goal::GetValue() {
	return _value;
}

void Goal::SetRateOfChange(float value) 
{
	_rateOfChange = value;
}

void Goal::SetValue(float value) 
{
	_value = value;
}

bool Goal::IsCritical() {
	return _value >= _threshold;
}

float Goal::Importance()
{
	return _value - _threshold;
}

void Goal::Update(float timeDelta) 
{
	_value += (timeDelta * _rateOfChange);
	if (_value <= 0)
		_value = 0;
	if (_value >= 1)
		_value = 1;

}

void Goal::ResetToDefault() 
{
	_rateOfChange = _defaultRateOfChange;
}
