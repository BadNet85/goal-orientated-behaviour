#pragma once
#include <string>
#include "Goal.h"
#include <map>
#include <vector>

using namespace std;

class Action {
public:
	Action(string name, float duration, int availableFrom, int availableTo);
	~Action();
	void AddGoalEffect(string goal, float effect);
	void Update(float timeDelta);
	void start(float startTime);
	float GetEffectForGoal(string goal);
	int TimeLeft();
	bool IsDone();
	string GetName();
	bool operator==(Action other);
	bool HasEffectOn(string goal);
	bool IsAvailable(vector<int> time);
	void SetDuration(float dur);
	float GetDuration();
	vector<float> GetGoalEffects();

private:
	float _duration;
	float _startingTime;
	float _stopWatch;
	string _name;
	map<string, float> _goalEffects;
	int _availableFrom;
	int _availableTo;
};

