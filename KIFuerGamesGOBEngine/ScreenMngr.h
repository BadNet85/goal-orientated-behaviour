#pragma once
#include <vector>
#include "Action.h"
#include "Goal.h"
#include <curses.h>
enum Mode 
{
	none = NULL,
	Critical,
	Discontent,
	DiscontentTime,
};

using namespace std;
class ScreenMngr {
public:
	ScreenMngr();
	~ScreenMngr();

	void PrintTime(vector<int> time);
	void PrintToDo(vector<Action> toDo, Action* active);
	void PrintGoals(vector<Goal> goals);
	void PrintAvailable(vector<Action> available);
	void PrintMode(Mode mode);
	void PrintDiscontent(float dis, vector<Goal*> krit, Action* best,float reduction);
	int GetIntput();
	void EnableDiscontentWindow(bool value);
private:
	WINDOW * _timeWin;
	WINDOW *_toDoWin;
	WINDOW *_goalsWin;
	WINDOW *_availWin;
	WINDOW *_modeWin;
	WINDOW *_discontentWin;
	bool disEnabled = false;
};

