#pragma once
#include <map>
#include <iostream>
#include <vector>
#include <unordered_set>

#include "Goal.h"
#include "Action.h"
#include "ScreenMngr.h"

using namespace std;

class GoalEngine {
public:
	GoalEngine(ScreenMngr* mngr);
	~GoalEngine();
	void Update(float timeDelta);
	void addAction(Action action);
	void removeAction(Action action);
	void addGoal(Goal goal);
	bool UpdateTime(float timeDelta);
	vector<int> GetTime();
	void ManageAvailableActions();
	void SetMode(int input);


private:
	Action FindBestactionDisTime();
	Action FindBestAction(Goal goal);
	Action FindBestActionDis();
	Action * _activeAction = nullptr;
	vector<Goal> _goals;
	vector<Action> _availableActions;
	vector<Action> _allActions;
	vector<Action> _toDoList;
	int _timeHrs = 12;
	int _timeSec = 0;
	int _timeDay = 1;
	float _accumulatedDelta = 0.0f;
	ScreenMngr* mngr;
	Mode _mode = Mode::Critical;
	vector<Mode> _modes;
	int _modeSelector;

};

