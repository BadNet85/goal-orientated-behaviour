#include "stdafx.h"
#include "GoalEngine.h"
#include <curses.h>
#include <algorithm>


GoalEngine::GoalEngine(ScreenMngr *mgr) 
{
	mngr = mgr;
	_modes.push_back(Mode::Critical);
	_modes.push_back(Mode::Discontent);
	_modes.push_back(Mode::DiscontentTime);
	_modeSelector = 0;
}


GoalEngine::~GoalEngine() {}

void GoalEngine::Update(float timeDelta) 
{

	if (UpdateTime(timeDelta)) 
	{
		ManageAvailableActions();
	}
	mngr->PrintTime(GetTime());
	mngr->PrintGoals(_goals);
	mngr->PrintMode(_mode);


	if (_activeAction == nullptr && _toDoList.size() != 0) 
	{
		_activeAction = new Action(_toDoList[0]);
		_toDoList.erase(_toDoList.begin());
		for (auto itr = _goals.begin(); itr != _goals.end(); itr++) {
			if (_activeAction->HasEffectOn(itr->GetName())) {
				itr->SetRateOfChange(_activeAction->GetEffectForGoal(itr->GetName()));
			}
		}
	}
	else if (_activeAction != nullptr) 
	{
		_activeAction->Update(timeDelta);
		if (_activeAction->IsDone()) {
			_activeAction = nullptr;
			for (auto itr = _goals.begin(); itr != _goals.end(); itr++) {
				itr->ResetToDefault();
			}
		}
	}
	mngr->PrintToDo(_toDoList, _activeAction);

	Goal * temp = nullptr;
	for (auto itr = _goals.begin(); itr != _goals.end(); itr++) 
	{

		itr->Update(timeDelta);
		if (temp == nullptr && itr->IsCritical()) 
		{
			temp = new Goal(*itr);
		}
		if (temp != nullptr && itr->IsCritical())
		{
			if(itr->Importance() > temp->Importance())
				temp = new Goal(*itr);
		}
	}
	if (temp != nullptr) {
		Action * best = nullptr;
		switch (_mode) 
		{
			case(Mode::Critical):
			{
				mngr->EnableDiscontentWindow(false);
				best = new Action(FindBestAction(*temp));
				break;
			}
			case(Mode::Discontent):
			{
				mngr->EnableDiscontentWindow(true);
				best = new Action(FindBestActionDis());
				break;
			}
			case(Mode::DiscontentTime):
			{
				mngr->EnableDiscontentWindow(true);
				best = new Action(FindBestactionDisTime());
				break;
			}

		}
		if (find(_toDoList.begin(), _toDoList.end(), *best) == _toDoList.end())
			_toDoList.push_back(*best);
		delete best;
	}
	delete temp;
}

void GoalEngine::addAction(Action action) 
{
	if (find(_allActions.begin(), _allActions.end(), action) == _allActions.end())
		_allActions.push_back(action);
}

void GoalEngine::removeAction(Action action) 
{
	auto itr = find(_allActions.begin(), _allActions.end(), action);
	if (itr != _allActions.end())
		_allActions.erase(itr);
}

void GoalEngine::addGoal(Goal goal) 
{
	_goals.push_back(goal);
}



bool GoalEngine::UpdateTime(float timeDelta) 
{
	bool updated = false;
	_accumulatedDelta += timeDelta;
	if (_accumulatedDelta >= 0.3f) 
	{
		_timeSec++;
		_accumulatedDelta = 0;
		updated = true;
	}
	if (_timeSec >= 60) 
	{
		_timeSec = 0;
		_timeHrs++;
		updated = true;
	}
	if (_timeHrs >= 24) {
		_timeHrs = 0;
		updated = true;
		_timeDay++;
	}
	return updated;
}

vector<int> GoalEngine::GetTime() {
	vector<int> time;
	time.push_back(_timeHrs);
	time.push_back(_timeSec);
	time.push_back(_timeDay);
	return time;
}

void GoalEngine::ManageAvailableActions() 
{

	_availableActions.clear();
	for (auto itr = _allActions.begin(); itr != _allActions.end(); itr++)
	{
		if (itr->IsAvailable(GetTime()))
		{
			_availableActions.push_back(*itr);
		}

	}
	mngr->PrintAvailable(_availableActions);

}

void GoalEngine::SetMode(int input) 
{
	Mode selected;
	switch (input)
	{
		case(KEY_UP):
		{
			_modeSelector++;
			break;
		}
		case(KEY_DOWN):
		{
			_modeSelector--;
			break;
		}
	}
	if (_modeSelector > _modes.size()-1) {
		_modeSelector = _modes.size()-1;
	}
	else if (_modeSelector < 0) {
		_modeSelector = 0;
	}
	this->_mode = _modes[_modeSelector];
}

Action GoalEngine::FindBestactionDisTime() 
{
	float discontent = 0.0f;
	float prognose = 0.0f;
	float minValue;
	vector<Goal*> critical;
	Action change("Temp", 0, 0, 24);
	vector<float> values;


	for (auto itr = _goals.begin(); itr != _goals.end(); itr++) {
		if (itr->IsCritical()) 
		{
			discontent += itr->GetValue();
			critical.push_back(new Goal(*itr));
		}
	}
	minValue = discontent;
	Action* selection = nullptr;
	for (auto itr = _availableActions.begin(); itr != _availableActions.end(); itr++) 
	{
		prognose = 0.0f;
		change.SetDuration(itr->GetDuration());
		for (auto itr_2 = critical.begin(); itr_2 != critical.end(); itr_2++) {


			if (itr->HasEffectOn((*itr_2)->GetName())) 
			{
				change.AddGoalEffect((*itr_2)->GetName(), itr->GetEffectForGoal((*itr_2)->GetName()));
			}
			else
			{
				change.AddGoalEffect((*itr_2)->GetName(), (*itr_2)->GetRateOfChange());
			}

		}

		values = change.GetGoalEffects();

		for each (float var in values) {
			prognose += var * change.GetDuration();
		}

		if ((discontent + prognose) <= minValue)
		{
			minValue = discontent + prognose;
			selection = new Action(*itr);
		}
	}
	mngr->PrintDiscontent(discontent, critical, selection, minValue);
	if (selection == nullptr)
		selection = new Action("Nothing", 0, 0, 24);
	return *selection;
}

Action GoalEngine::FindBestAction(Goal goal) {
	Action* temp = nullptr;
	Action* selection = nullptr;
	for (auto itr = _availableActions.begin(); itr != _availableActions.end(); itr++) {
		temp = new Action(*itr);
		if (!temp->HasEffectOn(goal.GetName()))
			continue;
		if (selection != nullptr) {
			if (temp->GetEffectForGoal(goal.GetName()) < selection->GetEffectForGoal(goal.GetName())) {
				selection = temp;
			}
		} else {
			selection = temp;
		}
	}
	return *selection;
}

Action GoalEngine::FindBestActionDis() 
{
	float discontent = 0.0f;
	vector<Goal*> critical;
	for (auto itr = _goals.begin(); itr != _goals.end(); itr++) 
	{
		if (itr->IsCritical()) {
			discontent += itr->GetValue();
			critical.push_back(new Goal(*itr));
		}
	}
	Action* selection = nullptr;
	float minValue = discontent;
	for (auto itr = _availableActions.begin(); itr != _availableActions.end(); itr++) 
	{
		float temp = 0.0f;
		for (auto itr_2 = critical.begin(); itr_2 != critical.end(); itr_2++)
		{
			if (itr->HasEffectOn((*itr_2)->GetName()))
			{
				temp += itr->GetEffectForGoal((*itr_2)->GetName());
			}
		}
		temp = discontent + temp;
		if (temp <= minValue)
		{
			minValue = temp;
			selection = new Action(*itr);
		}
	}
	mngr->PrintDiscontent(discontent, critical, selection, minValue);
	if (selection == nullptr)
		selection = new Action("Nothing", 0, 0, 24);
	return *selection;
}
