#pragma once
#include <string>
using namespace std;
class Goal {
public:
	Goal(string name, float start, float threshold, float rateOfChange);
	~Goal();
	string GetName();
	float GetThreshold();
	float GetRateOfChange();
	float GetValue();
	void SetRateOfChange(float value);
	void SetValue(float value);
	bool IsCritical();
	float Importance();
	void Update(float timeDelta);
	void ResetToDefault();
private:
	string _name;
	float _threshold;
	float _rateOfChange;
	float _value;
	float _defaultRateOfChange;
};

