# Goal oriented behaviour Simulation

Simulation of AI handling different goals in 3 Different modes: critical goal, discontent, discontent

### Prerequisites

none

### Controls:

       key            function
     Up Arrow         next mode
    Down Arrow        previous mode

### Modes

critical Goal:
    picks the action that satifies the current critical Goal the best.

discontent:
    looks at each critical goal and which action can satisfie all of them the best.

discontent time
    again looks at each critical goal but take the time a action takes to complete into consideration. The action, that satifies all critical Goals the best, when completed,
    is picked by the engine.